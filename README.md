#Visual Studio Essential Training
- A Tool For Developers

IDE - Integrated Devevlopment Environment

## Code Writing and Editing:
it eases the work of writitng code and markup
contains many language Specific types
- VB
- F#
- C#
- C++

## Project Types
- Create any Imaginable Project
- Windows 
- Console
- Service
- Web 
- Phone
- Store
- Dozens of Project templates
- Hundreds of Sample Project

## Debugging and Testing Tools
integrated debugging tools
troubleshoot problems, int the code

## Testing
Write tests to verify code corrections
Run the tests from Visual Studio

##Productivity Helpers
- Research the .net framework
- integrated help
- server monitoring tools
- code separation tools
- Visual Designers for building UI

##Compilers and Build Engines
- Compilers : produce executable files
- Build Engine : Manage the build/compile process
- Deployment : Installers and tools to deploy App and Sites

##Database Helpers
-Create test and manage DB Schema 
-Generate Sample Data

##Databound code
-tools for creating databound code 
-web/desktop & other types of applications

##ORM
Object Relational Mapping
* tools to generate ORM for an App
* Create from existing DB Schema
* Create from Code base
* Create from Architechtural Documents

##Project And Team Services
- tools for aiding development teams
- track bugs and work items
- access a source control System
- Create and access team/project online portal
- see details reports about project status

#Features in all paid Editions
##IDE
1. Multiple Monitor Support
2. Refactoring
3. Extensible WPF-based Environment
4. JavaScript and jQuery Support
5. Multi Targetting
6. Blend for Visual Studio
7. One Click web development
8. project solution compatibility with visual studio 2010 SP1
9. Light Switch
10. Model Resource Viewer

##Debugger and Diagnostics
1. Featured Debugger
2. Profiling
3. CodeMetrices
4. Static code Analysis
5. Graphic Designing
6. windows phone simulator
7. windows 8 simulator
8. Page Inspector [internal and web debugging]

## Development Platform Support
1. Web
2. Windows Desktop
3. Windows Store Application
4. Windows Phone
5. Office
6. Sharepoint
7. Cloud

##Testing
Unit Tests

## Arcitecture and Modeling
Dependency Graphs

##Azure
* Azure is Microsoft's Cloud Offering
* Azure roles are the unit of deployment
* Web Role
* Worker Role
* Virtual Machine Role

## Web Developer
- Use the latest jQuery & HTML5
- Improvements to HTML Editor
- Improvements to JavaScript Editor
- Improvements to CSS Editor
- page Inspector
- brings browser diagnostic tools into Visual Studio

#Visual Studio Platforms 2015
1. Cross Platform Mobile Apps in C# with Xamarin for Visual Studio
2. Cross Platform Mobile Apps in HTML/JavaScript with Apache Cordova
3. Cross Platform Mobile Games in C# with Unity
4. Cross Platform Apps and Libraries for Native C++
5. Universal Windows apps for any windows 10 device
![universal apps - one windows platform](https://i-msdn.sec.s-msft.com/en-us/library/bb386063.c86bcc54-ae26-47a4-b042-32109aa68bf3(v=vs.140).jpeg?f=255&MSPPError=-2147217396)
6. Web
7. Classic Destop And Windows Store

#Visual Studio 2015 Services
1. Azure mobile Services
2. Azure Storage
3. Office 365[mail, calendars, files, users & groups]
4. Salesforce

# Cross Platform Debugging Support
1. JavaScript/Cordova
2. C#/Xamarin
3. C++/Android

#Exploring Visual Studio
##Using Visual Designer & Toolbox
![Designer 1](http://i.imgur.com/V7dUdww.png)
![Designer 2](http://i.imgur.com/RLCFLwY.png)
